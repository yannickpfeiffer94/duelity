using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Card", menuName = "ScriptableObject/Card")]
public class CardSO : ScriptableObject
{
    public CardSide front, back;
}

[System.Serializable]
public class CardSide
{
    [SerializeField] CardType cardType;
    [SerializeField] string name;
    [SerializeField] Element element;
    [SerializeField] Sprite artwork;
    [SerializeField] List<SpellEffect> spellEffects;

    public CardType CardType { get => cardType; }
    public string Name { get => name; }
    public Element Element { get => element; }
    public Sprite Artwork { get => artwork; }
    public List<SpellEffect> SpellEffects { get => spellEffects; }
}

[System.Serializable]
public class SpellEffect
{
    [Range(0, 10)] public int requiredDevotion;
    public EffectType effectType;
    [Range(0, 3)] public int devotionModifier;
    [Range(0, 10)] public int basicModifier;
    public string effectDescription;

    public IEnumerator Resolve(Element element, int devotion, Player activePlayer, Player passivePlayer, bool ignoreArmor = false)
    {
        float delay = 0.5f;
        int value = devotionModifier * devotion + basicModifier;
        yield return new WaitForSeconds(delay);
        switch (effectType)
        {
            case EffectType.Heal:
                yield return activePlayer.Heal(value, delay);
                break;
            case EffectType.Damage:
                yield return passivePlayer.RecieveDamage(value, ignoreArmor, delay);
                break;
            case EffectType.Armor:
                yield return activePlayer.IncreaseArmor(value, delay);
                break;
            case EffectType.IncreaseDevotion:
                yield return activePlayer.IncreaseDevotion(element, value, delay);
                break;
            case EffectType.Flip:
                yield return new WaitForSeconds(delay);
                GameManager.Instance.currentState = GameManager.GameState.ChooseFlipCard;
                GameManager.Instance.UpdateActionText(activePlayer);
                GameManager.Instance.cardForFlip = null;
                while (GameManager.Instance.cardForFlip == null)
                {
                    yield return null;
                }
                GameManager.Instance.cardForFlip.FlipCard();
                SoundManager.Instance.PlaySound(SoundType.Flip);
                GameManager.Instance.currentState = GameManager.GameState.PlayCard;
                break;
        }
        yield return new WaitForSeconds(0.5f);
    }
}

public enum EffectType
{
    Damage,
    Heal,
    Flip,
    Freez,
    Armor,
    IncreaseDevotion

}

public enum CardType
{
    Permanent,
    Spell
}


public enum Element
{
    Fire,
    Water,
    Wind,
    Earth
}
