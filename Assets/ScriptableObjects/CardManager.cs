using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardManager : MonoBehaviour
{
    public static CardManager Instance;

    public CardHandler cardDeck, cardLeft, cardCenter, cardRight;

    public List<DeckListCardInfo> deckContentList = new List<DeckListCardInfo>();

    public List<Card> deck = new List<Card>();
    public List<Card> graveyard = new List<Card>();


    private void Awake()
    {
        Instance = this;

        InitializeDeck();
        deck.Shuffle();
        deck.ForEach(t => t.RandomizeSide());
        
        cardRight.SetCard(deck[0]);
        deck.RemoveAt(0);

        cardCenter.SetCard(deck[0]);
        deck.RemoveAt(0);

        cardLeft.SetCard(deck[0]);
        deck.RemoveAt(0);

        cardDeck.SetCard(deck[0]);
        deck.RemoveAt(0);
    }

    public void RemoveCard(CardHandler cardDrawer)
    {
        if(cardDrawer.card.currentSide.CardType == CardType.Spell)
            graveyard.Add(cardDrawer.card);
        cardDrawer.Hide();
    }

    public void DrawNext(CardHandler cardHandler)
    {
        if(deck.Count == 0)
        {
            foreach(Card card in graveyard)
            {
                deck.Add(card);
            }
            graveyard.Clear();

            deck.Shuffle();
            deck.ForEach(t => t.RandomizeSide());
        }

        cardHandler.Hide(false);
        cardHandler.SetCard(cardDeck.card);

        cardDeck.SetCard(deck[0]);
        deck.RemoveAt(0);
    }

    public void InitializeDeck()
    {
        foreach(DeckListCardInfo cardInfo in deckContentList)
        {
            for(int i = 0; i < cardInfo.amount; i++)
            {
                Card card = new Card(cardInfo.card);
                card.currentSide = cardInfo.card.front;
                deck.Add(card);
            }
        }
    }
}

static class Extensions
{
    ///https://stackoverflow.com/questions/273313/randomize-a-listt
    private static System.Random rng = new System.Random();
    public static void Shuffle<T>(this IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}

[System.Serializable]
public class DeckListCardInfo
{
    public CardSO card;
    public int amount;
}
