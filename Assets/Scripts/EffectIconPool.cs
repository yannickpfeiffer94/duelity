using UnityEngine;

public class EffectIconPool : MonoBehaviour
{
    public static EffectIconPool Instance;


    [Header("Water Sprites")]
    public Sprite waterHeal;
    public Sprite waterLock;
    public Sprite waterArmor;
    public Sprite waterDamage;
    public Sprite waterFlip;
    public Sprite waterDevotion;

    [Header("Fire Sprites")]
    public Sprite fireHeal;
    public Sprite fireLock;
    public Sprite fireArmor;
    public Sprite fireDamage;
    public Sprite fireFlip;
    public Sprite fireDevotion;

    [Header("Wind Sprites")]
    public Sprite windHeal;
    public Sprite windLock;
    public Sprite windArmor;
    public Sprite windDamage;
    public Sprite windFlip;
    public Sprite windDevotion;

    [Header("Earth Sprites")]
    public Sprite earthHeal;
    public Sprite earthLock;
    public Sprite earthArmor;
    public Sprite earthDamage;
    public Sprite earthFlip;
    public Sprite earthDevotion;

    private void Awake()
    {
        Instance = this;
    }

    public Sprite GetEffectSprite(Element element, EffectType effectType)
    {
        switch(element)
        {
            case Element.Fire:
                switch(effectType)
                {
                    case EffectType.Damage:
                        return fireDamage;
                    case EffectType.Armor:
                        return fireArmor;
                    case EffectType.Flip:
                        return fireFlip;
                    case EffectType.Freez:
                        return fireLock;
                    case EffectType.Heal:
                        return fireHeal;
                    case EffectType.IncreaseDevotion:
                        return fireDevotion;
                }
                break;
            case Element.Water:
                switch (effectType)
                {
                    case EffectType.Damage:
                        return waterDamage;
                    case EffectType.Armor:
                        return waterArmor;
                    case EffectType.Flip:
                        return waterFlip;
                    case EffectType.Freez:
                        return waterLock;
                    case EffectType.Heal:
                        return waterHeal;
                    case EffectType.IncreaseDevotion:
                        return waterDevotion;
                }
                break;
            case Element.Wind:
                switch (effectType)
                {
                    case EffectType.Damage:
                        return windDamage;
                    case EffectType.Armor:
                        return windArmor;
                    case EffectType.Flip:
                        return windFlip;
                    case EffectType.Freez:
                        return windLock;
                    case EffectType.Heal:
                        return windHeal;
                    case EffectType.IncreaseDevotion:
                        return windDevotion;
                }
                break;
            case Element.Earth:
                switch (effectType)
                {
                    case EffectType.Damage:
                        return earthDamage;
                    case EffectType.Armor:
                        return earthArmor;
                    case EffectType.Flip:
                        return earthFlip;
                    case EffectType.Freez:
                        return earthLock;
                    case EffectType.Heal:
                        return earthHeal;
                    case EffectType.IncreaseDevotion:
                        return earthDevotion;
                }
                break;
        }
        return null;
    }

}
