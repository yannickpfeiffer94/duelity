using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public Player playerLeft, playerRight;
    public int startingLifeTotal;
    public int startingDevotion;
    public GameObject gameOverScreen;
    public TextMeshProUGUI playerWonText;
    public TextMeshProUGUI actionText;

    public Player activePlayer;
    Player passivePlayer;
    public GameState currentState = GameState.ChooseCard;

    public CardHandler cardForFlip = null;

    private void Awake()
    {
        Instance = this;
        playerLeft.Init(startingLifeTotal, startingDevotion);
        playerRight.Init(startingLifeTotal, startingDevotion);

        activePlayer = playerLeft;
        passivePlayer = playerRight;

        UpdateActionText(activePlayer);
    }

    private void Start()
    {
        SetSideColors(activePlayer, passivePlayer);
    }

    public void UpdateActionText(Player player)
    {
        string playerName = (player == playerLeft ? "Left Player" : "Right Player");

        switch (currentState)
        {
            case GameState.ChooseCard:
                actionText.text = playerName + ": Choose a Card to play";
                break;
            case GameState.ChooseFlipCard:
                actionText.text = playerName + ": Chooses a card to flip";
                break;
            case GameState.PlayCard:
                actionText.text = "";
                break;
        }
    }

    public void PlayerDied(Player player)
    {
        gameOverScreen.SetActive(true);
        playerWonText.text = (player == playerLeft ? "Right Player " : "Left Player ") + "won!";
    }


    public IEnumerator PlayCard(CardHandler cardHandler)
    {
        currentState = GameState.PlayCard;
        UpdateActionText(activePlayer);
        Card card = cardHandler.card;
        CardManager.Instance.RemoveCard(cardHandler);

        Element element = card.currentSide.Element;

        if (card.currentSide.CardType == CardType.Spell)
        {
            int devotion = activePlayer.GetDevotion(card.currentSide.Element);

            yield return Activate(card.currentSide.SpellEffects, devotion, element, activePlayer, passivePlayer);

            if (activePlayer.permanentLeft.element == element && activePlayer.permanentLeft.isActive)
            {
                yield return Activate(activePlayer.permanentLeft.spellEffects, devotion, element, activePlayer, passivePlayer, true);
            }

            if (activePlayer.permanentRight.element == element && activePlayer.permanentLeft.isActive)
            {
                yield return Activate(activePlayer.permanentRight.spellEffects, devotion, element, activePlayer, passivePlayer, true);
            }



            if (passivePlayer.permanentLeft.element == element && passivePlayer.permanentLeft.isActive)
            {
                yield return Activate(passivePlayer.permanentLeft.spellEffects, devotion, element, passivePlayer, activePlayer, true);
            }

            if (passivePlayer.permanentRight.element == element && passivePlayer.permanentLeft.isActive)
            {
                yield return Activate(passivePlayer.permanentRight.spellEffects, devotion, element, passivePlayer, activePlayer, true);
            }

        }
        else
        {
            SoundManager.Instance.PlaySound(SoundType.Permanent);
            
            if (activePlayer.permanentLeft.isActive == false)
            {
                activePlayer.permanentLeft.Activate(element, card.currentSide);

            }
            else
            {
                activePlayer.permanentRight.Activate(element, card.currentSide);
            }
        }



        CardManager.Instance.DrawNext(cardHandler);
        SwapActivePlayer();
        currentState = GameState.ChooseCard;
        UpdateActionText(activePlayer);
    }

    IEnumerator Activate(List<SpellEffect> spellEffects, int devotion, Element element, Player activatingPlayer, Player passivePlayer, bool ignoreArmor = false)
    {
        SetSideColors(activatingPlayer, passivePlayer);

        List<SpellEffect> activatedSpellEffects = spellEffects
            .Where(t => t.requiredDevotion <= devotion)
            .ToList();

        foreach (SpellEffect spell in activatedSpellEffects)
        {
            activatingPlayer.spellEffect.enabled = true;
            activatingPlayer.spellEffect.sprite = EffectIconPool.Instance.GetEffectSprite(element, spell.effectType);
            activatingPlayer.spellEffect.SetNativeSize();
            yield return spell.Resolve(element, activatingPlayer.GetDevotion(element), activatingPlayer, passivePlayer, ignoreArmor);
            activatingPlayer.spellEffect.enabled = false;
        }
    }

    public void SwapActivePlayer()
    {
        Player tmpPlayer = activePlayer;
        activePlayer = passivePlayer;
        passivePlayer = tmpPlayer;

        SetSideColors(activePlayer, passivePlayer);

        actionText.text = (activePlayer == playerLeft ? playerLeft : playerRight) + "choose a Card to play";
    }

    public void SetSideColors(Player activePlayer, Player passivePlayer)
    {
        activePlayer.side.color = ColorManager.Instance.activePlayerColor;
        passivePlayer.side.color = ColorManager.Instance.passivePlayerColor;
    }


    public enum GameState
    {
        ChooseCard,
        PlayCard,
        ChooseFlipCard,
        ChooseLockCard
    }
}
