using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Card
{
    public CardSide front, back;

    public CardSide currentSide;

    public Card(CardSO cardSO)
    {
        front = cardSO.front;
        back = cardSO.back;
    }

    public void SwitchSides()
    {
        currentSide = front == currentSide ? back : front;
    }

    public void RandomizeSide()
    {
        currentSide = Random.Range(0, 2) == 0 ? front : back;
    }
}
