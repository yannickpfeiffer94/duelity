﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public partial class CardHandler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    public Card card;

    public IconPool iconPool;

    public Image frame;
    public Image artwork;
    public Image cardType;
    public new TextMeshProUGUI name;
    public CardDescriptionRenderer cardDescriptionRenderer;
    public TooltipTriggerer tooltipTriggerer;

    public bool isActive = true;

    Vector3 originalScale;
    private void Awake()
    {
        UpdateCardDisplay(card);
        originalScale = transform.localScale;
    }

    public void Hide(bool hide = true)
    {
        isActive = !hide;

        frame.gameObject.SetActive(!hide);
        artwork.gameObject.SetActive(!hide);
        cardType.gameObject.SetActive(!hide);
        name.gameObject.SetActive(!hide);
        cardDescriptionRenderer.gameObject.SetActive(!hide);
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            FlipCard();
        }
    }

    public void SetCard(Card card)
    {
        this.card = card;
        UpdateCardDisplay(card);

    }

    private void UpdateTooltip(CardSide cardSide)
    {

        if (cardSide.CardType == CardType.Permanent)
        {
            tooltipTriggerer.tooltipHeader = "Permanent";
            tooltipTriggerer.tooltipDescription = "Effect gets executed whenever a card of the same element is played.";
        }
        if (cardSide.CardType == CardType.Spell)
        {
            tooltipTriggerer.tooltipHeader = "Spell";
            tooltipTriggerer.tooltipDescription = "Effect gets executed immedeately.";
        }
    }



    public void FlipCard()
    {
        card.SwitchSides();
        UpdateCardDisplay(card);
    }

    public void UpdateCardDisplay(Card card)
    {
        CardSide cardSide = card.currentSide;
        cardType.sprite = iconPool.GetCardTypeSprite(cardSide.Element, cardSide.CardType);
        artwork.sprite = cardSide.Artwork;
        name.text = cardSide.Name;
        cardDescriptionRenderer.RenderCardDescription(cardSide);
        frame.sprite = iconPool.GetFrameSprite(cardSide.Element);

        UpdateTooltip(cardSide);
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        LeanTween.scale(gameObject, new Vector3(1.1f, 1.1f), 0.15f);
        SoundManager.Instance.PlaySound(SoundType.Hover);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        LeanTween.scale(gameObject, originalScale, 0.15f);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (GameManager.Instance.currentState == GameManager.GameState.ChooseCard)
        {
            if ((!GameManager.Instance.activePlayer.permanentRight.isActive 
                || card.currentSide.CardType != CardType.Permanent)
                && CardManager.Instance.cardDeck != this)
                StartCoroutine(GameManager.Instance.PlayCard(this));
            else
                SoundManager.Instance.PlaySound(SoundType.Error);
        }
        else if (GameManager.Instance.currentState == GameManager.GameState.ChooseFlipCard
            && isActive)
        {
            GameManager.Instance.cardForFlip = this;
        }
    }
}
