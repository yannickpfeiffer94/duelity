﻿using UnityEngine;

public class IconPool : MonoBehaviour
{
    public Sprite windFrame, waterFrame, fireFrame, earthFrame;
    public Sprite windPermanent, waterPermanent, firePermanent, earthPermanent;
    public Sprite windSpell, waterSpell, fireSpell, earthSpell;

    public Sprite GetFrameSprite(Element element)
    {
        switch (element)
        {
            case Element.Fire:
                return fireFrame;
            case Element.Water:
                return waterFrame;
            case Element.Wind:
                return windFrame;
            case Element.Earth:
                return earthFrame;
        }

        return null;
    }

    public Sprite GetCardTypeSprite(Element element, CardType cardType)
    {
        switch (cardType)
        {
            case CardType.Permanent:
                switch(element)
                {
                    case Element.Fire:
                        return firePermanent;
                    case Element.Water:
                        return waterPermanent;
                    case Element.Wind:
                        return windPermanent;
                    case Element.Earth:
                        return earthPermanent;
                }
                break;
            case CardType.Spell:
                switch (element)
                {
                    case Element.Fire:
                        return fireSpell;
                    case Element.Water:
                        return waterSpell;
                    case Element.Wind:
                        return windSpell;
                    case Element.Earth:
                        return earthSpell;
                }
                break;
        }

        return null;
    }
}