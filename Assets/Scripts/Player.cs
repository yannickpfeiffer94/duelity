using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public int fireDevotion, waterDevotion, windDevotion, earthDevotion;

    public int lifeTotal;
    public int armor;

    public TextMeshProUGUI fireDevotionText, waterDevotionText, windDevotionText, earthDevotionText;
    public TextMeshProUGUI lifeTotalText;
    public TextMeshProUGUI armorText;


    public Permanent permanentLeft;
    public Permanent permanentRight;

    public Image side;
    public Image spellEffect;

    public void Init(int startingLifeTotal, int startingDevotion)
    {
        lifeTotal = startingLifeTotal;
        lifeTotalText.text = lifeTotal.ToString();

        fireDevotion = startingDevotion;
        waterDevotion = startingDevotion;
        windDevotion = startingDevotion;
        earthDevotion = startingDevotion;
        fireDevotionText.text = fireDevotion.ToString();
        waterDevotionText.text = waterDevotion.ToString();
        windDevotionText.text = windDevotion.ToString();
        earthDevotionText.text = earthDevotion.ToString();

        armor = 0;
        armorText.text = armor.ToString();
    }

    public IEnumerator Heal(int value, float delay)
    {
        lifeTotalText.color = ColorManager.Instance.highlightTextColorPositiveEffect;
        yield return new WaitForSeconds(delay);
        lifeTotal += value;
        lifeTotalText.text = lifeTotal.ToString();
        SoundManager.Instance.PlaySound(SoundType.Heal);
        yield return new WaitForSeconds(delay);
        lifeTotalText.color = ColorManager.Instance.basicTextColor;
    }

    public IEnumerator RecieveDamage(int value, bool ignoreArmor = false, float delay = 0)
    {
        lifeTotalText.color = ColorManager.Instance.highlightTextColorNegativeEffect;
        yield return new WaitForSeconds(delay);

        if (ignoreArmor)
            lifeTotal -= value;
        else
            lifeTotal -= Mathf.Max(0, value - armor);

        SoundManager.Instance.PlaySound(SoundType.Damage);
        lifeTotalText.text = lifeTotal.ToString();

        yield return new WaitForSeconds(delay);
        lifeTotalText.color = ColorManager.Instance.basicTextColor;

        if (lifeTotal <= 0)
            GameManager.Instance.PlayerDied(this);
    }

    public IEnumerator IncreaseArmor(int value = 1, float delay = 0)
    {
        armorText.color = ColorManager.Instance.highlightTextColorPositiveEffect;
        yield return new WaitForSeconds(delay);
        armor += value;
        armorText.text = armor.ToString();
        SoundManager.Instance.PlaySound(SoundType.Armor);
        yield return new WaitForSeconds(delay);
        armorText.color = ColorManager.Instance.basicTextColor;
    }

    public IEnumerator IncreaseDevotion(Element element, int value, float delay)
    {
        switch (element)
        {
            case Element.Fire:
                fireDevotionText.color = ColorManager.Instance.highlightTextColorPositiveEffect;
                yield return new WaitForSeconds(delay);
                fireDevotion += value;
                fireDevotionText.text = fireDevotion.ToString();
                break;
            case Element.Wind:
                windDevotionText.color = ColorManager.Instance.highlightTextColorPositiveEffect;
                yield return new WaitForSeconds(delay);
                windDevotion += value;
                windDevotionText.text = windDevotion.ToString();
                break;
            case Element.Water:
                waterDevotionText.color = ColorManager.Instance.highlightTextColorPositiveEffect;
                yield return new WaitForSeconds(delay);
                waterDevotion += value;
                waterDevotionText.text = waterDevotion.ToString();
                break;
            case Element.Earth:
                earthDevotionText.color = ColorManager.Instance.highlightTextColorPositiveEffect;
                yield return new WaitForSeconds(delay);
                earthDevotion += value;
                earthDevotionText.text = earthDevotion.ToString();
                break;
        }

        SoundManager.Instance.PlaySound(SoundType.Devotion);
        yield return new WaitForSeconds(delay);
        earthDevotionText.color = ColorManager.Instance.basicTextColor;
        waterDevotionText.color = ColorManager.Instance.basicTextColor;
        windDevotionText.color = ColorManager.Instance.basicTextColor;
        fireDevotionText.color = ColorManager.Instance.basicTextColor;
    }

    public int GetDevotion(Element element)
    {
        switch (element)
        {
            case Element.Fire:
                return fireDevotion;
            case Element.Water:
                return waterDevotion;
            case Element.Wind:
                return windDevotion;
            default:
                return earthDevotion;
        }
    }
}
