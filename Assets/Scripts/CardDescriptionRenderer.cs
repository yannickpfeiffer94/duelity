using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class CardDescriptionRenderer : MonoBehaviour
{
    [SerializeField] private GameObject effectContainer;

    public void RenderCardDescription(CardSide cardSide)
    {
        List<SpellEffect> effects = cardSide.SpellEffects;
        RemoveExistingEffectContainers();
        Dictionary<float, List<SpellEffect>> devotionDirectory = GroupByDevotion(effects);

        foreach (var devotion in devotionDirectory)
        {
            SpawnEffectContainer(devotion, cardSide.Element);
        }
    }


    private Dictionary<float, List<SpellEffect>> GroupByDevotion(List<SpellEffect> effects)
    {
        List<float> devotions = new List<float>();
        for (int i = 0; i < effects.Count; i++)
        {
            devotions.Add(effects[i].requiredDevotion);

        }
        devotions = devotions.Distinct().ToList();
        Dictionary<float, List<SpellEffect>> devotionDictionary = new Dictionary<float, List<SpellEffect>>();

        foreach (var devotion in devotions)
        {
            List<SpellEffect> effectsByDevotion = new List<SpellEffect>();

            foreach (var effect in effects)
            {
                if (effect.requiredDevotion == devotion)
                {
                    effectsByDevotion.Add(effect);
                }
            }
            devotionDictionary.Add(devotion, effectsByDevotion);
        }
        return devotionDictionary;
    }

    private void RemoveExistingEffectContainers()
    {
        foreach (Transform child in gameObject.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    private void SpawnEffectContainer(KeyValuePair<float, List<SpellEffect>> devotion, Element element)
    {
        GameObject effectContainerInstance = Instantiate(effectContainer);
        effectContainerInstance.transform.SetParent(gameObject.transform);
        effectContainerInstance.transform.localScale = Vector3.one;

        EffectRenderer effectRenderer = effectContainerInstance.GetComponent<EffectRenderer>();
        effectRenderer.RenderEffect(devotion, element);
    }
}
