using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TooltipManager : MonoBehaviour
{
    public static TooltipManager Instance;
    public Tooltip tooltip;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);
    }

    public void Show(string header, string content)
    {
        Instance.tooltip.SetText(header, content);
        Instance.tooltip.gameObject.SetActive(true);
        LeanTween.scale(Instance.tooltip.gameObject, new Vector3(1f, 1f), 0.17f);
    }

    public IEnumerator Hide()
    {
        float tweenTime = 0.13f;
        LeanTween.scale(Instance.tooltip.gameObject, new Vector3(0f, 0f), tweenTime);
        yield return new WaitForSeconds(tweenTime + 0.01f);
        Instance.tooltip.gameObject.SetActive(false);
    }
}
