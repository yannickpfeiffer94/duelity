using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorManager : MonoBehaviour
{
    public static ColorManager Instance;

    public Color passivePlayerColor;
    public Color activePlayerColor;
    public Color highlightTextColorNegativeEffect;
    public Color highlightTextColorPositiveEffect;
    public Color basicTextColor = Color.black;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }
}
