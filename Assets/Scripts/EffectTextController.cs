﻿using System.Collections;
using UnityEngine;


public static class EffectTextController
{

    public static string GetSpellEffectText(SpellEffect effect, Element element)
    {
        if(effect.effectType == EffectType.Flip)
        {
            return effect.basicModifier + "x " + GetEffectTypeText(effect.effectType, element);
        }
        if (effect.effectType == EffectType.IncreaseDevotion)
        {
            return GetEffectTypeText(effect.effectType, element) + "+" + effect.basicModifier;
        }
        else
        {
            string devotionModifier = effect.devotionModifier > 0 ? effect.devotionModifier + "x" + GetElementInlineSprite(element) : "";
            string basicModifier = effect.basicModifier > 0 ? effect.basicModifier.ToString() : "";

            return GetEffectTypeText(effect.effectType, element) + "=" + devotionModifier + basicModifier;
        }
    }

    public static string GetElementInlineSprite(Element element)
    {
        return "<sprite name=\"" + GetElementText(element) + "\">";
    }

    public static string GetEffectTypeText(EffectType effectType, Element element)
    {
        switch(effectType)
        {
            case EffectType.Damage:
                return "<sprite name=\"damage_" + GetElementText(element) + "\">";
            case EffectType.Heal:
                return "<sprite name=\"heal_" + GetElementText(element) + "\">";
            case EffectType.Armor:
                return "<sprite name=\"shild_" + GetElementText(element) + "\">";
            case EffectType.Flip:
                return "<sprite name=\"flip_" + GetElementText(element) + "\">";
            case EffectType.IncreaseDevotion:
                return GetElementInlineSprite(element);
        }
        return "ERROR";
    }

    public static string GetElementText(Element element)
    {
        switch(element)
        {
            case Element.Earth:
                return "earth";
            case Element.Wind:
                return "wind";
            case Element.Fire:
                return "fire";
            case Element.Water:
                return "water";
        }
        return "ERROR";
    }
}
