using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TooltipTriggerer : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public string tooltipHeader;
    public string tooltipDescription;
    public TooltipType tooltipType;

    Coroutine timer;

    public void OnPointerEnter(PointerEventData eventData)
    {
        timer = StartCoroutine(StartToolTip());
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (timer != null)
            StopCoroutine(timer);
        StartCoroutine(TooltipManager.Instance.Hide());
    }

    IEnumerator StartToolTip()
    {
        yield return new WaitForSeconds(0.5f);
        TooltipManager.Instance.Show(tooltipHeader, tooltipDescription);
    }
}


public enum TooltipType
{
    PermanentCardInfo,
    IconTypeInfo
}
