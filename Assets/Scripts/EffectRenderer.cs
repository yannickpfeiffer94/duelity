using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EffectRenderer : MonoBehaviour
{
    public Sprite waterIcon, fireIcon, windIcon, earthIcon;
    public GameObject textToRender;
    public GameObject wrapperContainer;
    public Image elementImage;
    public GameObject devotionParent;
    public GameObject bla;
    public TextMeshProUGUI requiredDevotion;

    public void RenderEffect(KeyValuePair<float, List<SpellEffect>> devotion, Element element)
    {

        /*if(devotion.Key == 0)
        {
            devotionParent.SetActive(false); ;
        }

        Sprite effectSprite = EffectIconPool.Instance.GetEffectSprite(element, EffectType.IncreaseDevotion);
        elementImage.sprite = effectSprite;
        requiredDevotion.text = ">" + (devotion.Key -1).ToString();

        requiredDevotion.color = element == Element.Wind ? Color.black : Color.white;*/
        


/*        float minTextSize = 1000f;
        List<TextMeshProUGUI> effectTexts = new List<TextMeshProUGUI>();*/
/*
        List<int> differentDevotionRequirements = new List<int>();

        foreach (var effect in devotion.Value)
        {
            if (!differentDevotionRequirements.Contains(effect.requiredDevotion))
                differentDevotionRequirements.Add(effect.requiredDevotion);
        }

        foreach(int devotionRequirement in differentDevotionRequirements)
        {
            foreach(var effect in de)
        }*/
        GameObject textToRenderInstance = Instantiate(textToRender);
        TextMeshProUGUI effectText = textToRenderInstance.GetComponent<TextMeshProUGUI>();
        effectText.text = "";
        textToRenderInstance.transform.SetParent(transform);
        textToRenderInstance.transform.localScale = Vector3.one;

        foreach (var effect in devotion.Value)
        {
            if(!effectText.text.Equals(""))
            {
                effectText.text += " & ";
            }
            else if(effect.requiredDevotion > 0)
            {
                effectText.text += EffectTextController.GetElementInlineSprite(element) + ">" + (effect.requiredDevotion - 1) + " also ";
            }
            effectText.text += EffectTextController.GetSpellEffectText(effect, element);  //effect.effectDescription;
            effectText.color = Color.black; //element == Element.Wind ? Color.black : Color.white;



/*            if (minTextSize > effectText.fontSize)
                minTextSize = effectText.fontSize;
            effectTexts.Add(effectText);*/
        }

        /*foreach(var effectText in effectTexts)
        {
            effectText.enableAutoSizing = false;
            effectText.fontSize = minTextSize;
        }*/
        //StartCoroutine(SetSize(effectTexts));
    }

    IEnumerator SetSize(List<TextMeshProUGUI> effectTexts)
    {
        yield return new WaitForEndOfFrame();
        float minTextSize = 1000f;

        foreach( var effectText in effectTexts)
        {
            if (minTextSize > effectText.fontSize)
                minTextSize = effectText.fontSize;
        }

        foreach (var effectText in effectTexts)
        {
            effectText.enableAutoSizing = false;
            effectText.fontSize = minTextSize;
        }
    }
}
