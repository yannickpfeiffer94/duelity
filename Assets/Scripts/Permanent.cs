﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Permanent : MonoBehaviour
{
    public bool isActive = false;
    public Image image;
    public TooltipTriggerer tooltipTriggerer;
    public GameObject tooltipTriggerGO;

    public Element element;
    public List<SpellEffect> spellEffects = new List<SpellEffect>();

    public void Activate(Element element, CardSide cardSide)
    {
        isActive = true;
        this.element = element;
        this.spellEffects = cardSide.SpellEffects;
        SetImage(element, spellEffects[0].effectType);
        SetTooltip(cardSide);
        tooltipTriggerGO.SetActive(true);
    }

    public void SetImage(Element element, EffectType effectType)
    {
        image.enabled = true;
        image.sprite = EffectIconPool.Instance.GetEffectSprite(element, effectType);
    }

    private void SetTooltip(CardSide cardSide)
    {
        tooltipTriggerer.tooltipHeader = cardSide.Name;
        bool damageHint = false;


        tooltipTriggerer.tooltipDescription = "Whenerver <u>any</u> player plays a " + EffectTextController.GetElementInlineSprite(cardSide.Element) + " spell: \n";
        foreach (var effect in cardSide.SpellEffects)
        {
            string requirement = effect.requiredDevotion > 0 ? EffectTextController.GetElementInlineSprite(cardSide.Element) + ">" + (effect.requiredDevotion - 1) + ": " : "";

            tooltipTriggerer.tooltipDescription += requirement + EffectTextController.GetSpellEffectText(effect, cardSide.Element) + "\n";
            if (effect.effectType == EffectType.Damage)
                damageHint = true;
        }

        if(damageHint)
            tooltipTriggerer.tooltipDescription += "\n (<sprite name=\"damage_" + EffectTextController.GetElementText(cardSide.Element) + "\"> caused by shrines ignores Armor)";
    }
}
