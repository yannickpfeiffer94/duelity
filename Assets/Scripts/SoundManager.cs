using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;
    public List<SoundItem> sounds;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            PlaySound(SoundType.Background);
        }
        else
            Destroy(gameObject);
    }

    public void PlaySound(SoundType soundType, float delay = 0f)
    {
        AudioSource audioSource = sounds.Where(t => t.soundType == soundType).FirstOrDefault().audioSource;
        audioSource.PlayDelayed(delay);
    }
}

public enum SoundType
{
    Heal,
    Damage,
    Armor,
    Flip,
    Lock,
    Win,
    Devotion,
    Hover,
    Error,
    Select,
    Permanent,
    Background
}

[System.Serializable]
public class SoundItem
{
    public AudioSource audioSource;
    public SoundType soundType;
}
