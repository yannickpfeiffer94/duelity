using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Tooltip : MonoBehaviour
{
    public TextMeshProUGUI header;
    public TextMeshProUGUI description;

    public void SetText(string header, string content)
    {
        SetHeader(header);
        SetDescription(content);
    }

    private void SetHeader(string header)
    {
        if (string.IsNullOrEmpty(header))
        {
            this.header.gameObject.SetActive(false);
        }
        else
        {
            this.header.gameObject.SetActive(true);
            this.header.text = header;
        }
    }

    private void SetDescription(string description)
    {
        if (string.IsNullOrEmpty(description))
        {
            this.description.gameObject.SetActive(false);
        }
        else
        {
            this.description.gameObject.SetActive(true);
            this.description.text = description;
        }
    }

    private void OnEnable()
    {
        Vector2 position = Input.mousePosition;
        gameObject.transform.position = position - new Vector2(0, 60);
    }

    public void Update()
    {
        
    }
}
